﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shout.Lib;

namespace Shout.Api.Controllers
{
    [EnableCors("Cors")]
    public class ApiController : Controller
    {
    	
        [HttpPost]
        public string Index(string text)
        {
            return LibUppercase.Uppercase(text);
        }
    }
}
